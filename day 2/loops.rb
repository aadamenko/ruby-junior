#!/usr/bin/ruby
# -*- coding: utf-8 -*-
# example 4 

=begin
 anonim block
=end
def myfunc2 a
  puts "Start #{a}"
  yield 12, 23
  puts "End #{a}"
end

=begin
  block to func
=end
def myfunc a, &b
  puts "Start #{a}"
  b.call 12, 23
  puts "End #{a}"
end

=begin
  run func with block argument
=end
myfunc :first do |x, y|
  puts "#{x}"
  puts "#{y}"
end

=begin
  lambda func
=end
func = lambda { |a, b| a+b*b}
puts func.call(1,2)