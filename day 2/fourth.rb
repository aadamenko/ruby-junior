#!/usr/bin/ruby
# -*- coding: utf-8 -*-
# example 8


=begin
  arguments of function
=end
def test1( a, b, c)
  puts "#{a}, #{b}, #{c}"
end

test1 1, 2, 3
l = [ 10, 20]
test1 1, *l
test1 *l, 1

=begin
  arguments of function 2
=end
def test2 a, b, *other
  puts "#{a}, #{b}, #{other}"
end

test2 1, 2, 'V', true, 10

=begin
  arguments of function 3 
=end
def test3 a, other
  puts "#{a}, #{other}"
end

test3 1, {:one => 'One', :two => 'Two'}
test3 1, :one => 'One', :two => 'Two'
test3 1, one: 'One', two: 'Two'
test3 1, {one: 'One', two: 'Two'}
