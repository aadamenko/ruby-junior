#!/usr/bin/ruby
# -*- coding: utf-8 -*-
# example 7

#proccess fernhulsta

def fern delta, x=0.5
  k = 0
  loop do 
    yield k, x
    x += x * ( 1.0-x ) * delta
    k += 1
  end
end

File.open( 'data.txt', 'wt' ) do |file|
  fern(0.5) do |k, x|
    file.puts "#{k}: #{x}"
    break if k > 20
  end
end

