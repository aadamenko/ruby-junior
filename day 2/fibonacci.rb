#!/usr/bin/ruby
# -*- coding: utf-8 -*-
# example 5


=begin
  fibonacchi vector
=end
def fibonacci n
  a, b = 1, 1
  n.times do
    yield a
    a, b = b, a+b
  end
end

fibonacci 10 do |num|
  puts num
  #break stop cycle
  #redo repeat step cycle
  #next next step of cycle
end

#list
list = [ 'one', 'two', :three]
list.each do |elem|
  puts elem
end

#hash
dict = { one: 1, two: 2, three: 3}
dict.each do |key, value|
  puts "#{key}: #{value}"
end

#1..5
1.upto(5) do |k|
  puts k
end

File.open( 'data.txt', 'rt').each do |line|
  puts line
end