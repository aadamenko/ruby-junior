#!/usr/bin/ruby
# -*- coding: utf-8 -*-
# example 3

module SqEq

  def solve( aa, bb, cc)
    d = bb*bb - 4.0 * aa * cc
    return [] if d < 0
    x1 = ( - bb + Math.sqrt(d) ) / ( aa * 2.0)
    x2 = ( - bb - Math.sqrt(d) ) / ( aa * 2.0)
    return [ x1, x2 ]
  end

end