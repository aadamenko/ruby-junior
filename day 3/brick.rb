#!/usr/bin/ruby
# *-* coding: utf-8 *-*

require_relative 'volumized'

class Brick
  
  #insert out module
  include Volumized
  
  # getters
  attr_reader :wid, :hei, :len
  #getters & setters
  #attr_accessor
  
=begin
    construct of class
=end
  def initialize( len, wid, hei )
    @len = len
    @wid = wid
    @hei = hei
  end
  
  #method of class

  
=begin
    part of brick
=end
  def part quant
    l1 = @len * quant
    @len -= l1
    return Brick.new( l1, @wid, @hei )
  end
  
=begin
    getter
=end
  def volume
    @len * @wid * @hei
  end
  
=begin
    setter
=end
  def volume= value
    @len = Math.cbrt( value )
    @wid = @len
    @hei = @len
  end
  
  def to_s
    result = super
    result + sprintf( '%.3fx%.3fx%.3f', @len, @wid, @hei )
  end
  
  
end
