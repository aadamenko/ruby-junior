#!/usr/bin/ruby
# *-* coding: utf-8 *-*

module Volumized
  
  
  def volume
    @volume = @len * @wid * @hei if @volume == nil
    @volume
  end
  
  def update
    @volume = nil
  end
end