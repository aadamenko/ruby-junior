#!/usr/bin/ruby
# -*- coding: utf-8 -*-
# example 2

print 'input a: '; a = Float( gets )
print 'input b: '; b = Float( gets )
print 'input c: '; c = gets.to_f

d = b*b - 4.0*a*c

if d >= 0 then
  x1 = ( - b + Math.sqrt(d) ) / ( a * 2.0)
  x2 = ( - b - Math.sqrt(d) ) / ( a * 2.0)
  puts x1, x2
else
  puts 'No solutions!'
end